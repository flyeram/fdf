/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:46:20 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/12 13:35:08 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <libft.h>
#include <unistd.h>
#include <fdf.h>
#include <stdlib.h>

void		executor(int ac, char **av, t_env *env)
{
	env->move->z = (int)((ft_min(env->win_size.x, env->win_size.y) /
	ft_max(env->sizey_ar, env->sizex_ar) / 1.5)) + 1;
	env->move->w = 0.1;
	env->a_color.color_a = (ac > 2) ? ft_hextodec(av[2]) : 0x0000FF;
	env->a_color.color_b = (ac > 3) ? ft_hextodec(av[3]) : 0x00FFFF;
	env->a_color.color_c = (ac > 4) ? ft_hextodec(av[4]) : 0xFFA500;
	env->a_color.color_d = (ac > 5) ? ft_hextodec(av[5]) : 0xFF0000;
	env->ac = ac;
	env->av = av;
	set_origin(env);
	center(env);
	mlx_expose_hook((*env).win, draw, env);
	mlx_hook((*env).win, 2, (1L << 0), press_key, env);
	mlx_loop((*env).mlx);
}

int			main(int ac, char **av)
{
	t_env	*env;
	int		win_x;
	int		win_y;

	if (ac < 2 || ac > 8)
	{
		ft_putstr("ERROR\n");
		return (0);
	}
	win_x = (ac > 6) ? ft_atoi(av[6]) : 2560;
	win_y = (ac > 7) ? ft_atoi(av[7]) : 1400;
	env = constructor_env(win_x, win_y);
	if (constructor(env, av[1]) == 0)
	{
		ft_putstr("ERROR\n");
		return (0);
	}
	executor(ac, av, env);
	return (0);
}
