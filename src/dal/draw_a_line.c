/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_a_line.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/17 14:53:51 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/08 15:17:18 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <fdf.h>
#include <libft.h>

void	draw_vertical(t_coords pt_a, t_coords pt_b, t_env env)
{
	float	min;
	float	max;

	min = ft_min(pt_a.y, pt_b.y);
	max = ft_max(pt_a.y, pt_b.y);
	while (min <= max)
	{
		image_put_pixel(env, pt_a.x, min, env.a_color.color_a);
		min++;
	}
}

void	draw_horizontal(t_coords pt_a, t_coords pt_b, t_env env)
{
	float	min;
	float	max;

	min = ft_min(pt_a.x, pt_b.x);
	max = ft_max(pt_a.x, pt_b.x);
	while (min <= max)
	{
		image_put_pixel(env, min, pt_a.y, env.a_color.color_a);
		min++;
	}
}

void	draw_a_line(t_coords pt_a, t_coords pt_b, t_env env, unsigned int color)
{
	int		dx_dy[3];

	env.a_color.color_a = color;
	if ((dx_dy[0] = ((int)pt_b.x - (int)pt_a.x)) > 0)
	{
		if ((dx_dy[1] = ((int)pt_b.y - (int)pt_a.y)) > 0)
			draw_dxp_dyp(pt_a, pt_b, env, dx_dy);
		else if (dx_dy[1] < 0)
			draw_dxp_dyn(pt_a, pt_b, env, dx_dy);
		else
			draw_horizontal(pt_a, pt_b, env);
	}
	else if (dx_dy[0] < 0)
	{
		if ((dx_dy[1] = ((int)pt_b.y - (int)pt_a.y)) > 0)
			draw_dxn_dyp(pt_a, pt_b, env, dx_dy);
		else if (dx_dy[1] < 0)
			draw_dxn_dyn(pt_a, pt_b, env, dx_dy);
		else
			draw_horizontal(pt_a, pt_b, env);
	}
	else
		draw_vertical(pt_a, pt_b, env);
}
