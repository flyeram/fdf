/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_a_line_4.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 15:16:14 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/08 15:16:23 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

void	loop_dxn_dyp_one(int *dx_dy, t_env env, t_coords pt_a, t_coords pt_b)
{
	while (pt_a.x >= pt_b.x)
	{
		image_put_pixel(env, pt_a.x, pt_a.y, env.a_color.color_a);
		pt_a.x--;
		if ((dx_dy[2] += dx_dy[1]) >= 0)
		{
			pt_a.y++;
			dx_dy[2] += dx_dy[0];
		}
	}
}

void	loop_dxn_dyp_two(int *dx_dy, t_env env, t_coords pt_a, t_coords pt_b)
{
	while (pt_a.y <= pt_b.y)
	{
		image_put_pixel(env, pt_a.x, pt_a.y, env.a_color.color_a);
		pt_a.y++;
		if ((dx_dy[2] += dx_dy[0]) < 0)
		{
			pt_a.x--;
			dx_dy[2] += dx_dy[1];
		}
	}
}

void	draw_dxn_dyp(t_coords pt_a, t_coords pt_b, t_env env, int *dx_dy)
{
	if ((-1 * dx_dy[0]) >= dx_dy[1])
	{
		dx_dy[1] *= 2;
		dx_dy[2] = dx_dy[0];
		dx_dy[0] = dx_dy[2] * 2;
		loop_dxn_dyp_one(dx_dy, env, pt_a, pt_b);
	}
	else
	{
		dx_dy[0] *= 2;
		dx_dy[2] = dx_dy[1];
		dx_dy[1] = dx_dy[2] * 2;
		loop_dxn_dyp_two(dx_dy, env, pt_a, pt_b);
	}
}
