/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_point.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 11:02:22 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/12 11:13:40 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>
#include <unistd.h>
#include <mlx.h>

void		draw_x(int x, int y, t_env *env, t_coords pt_a)
{
	t_coords	pt_b;

	pt_b = trans_point((*env).array[y][x + 1], env);
	if (pt_a.z <= 0 && pt_a.z == pt_b.z)
		draw_a_line(pt_a, pt_b, *env, env->a_color.color_a);
	else if (pt_a.z == pt_b.z)
		draw_a_line(pt_a, pt_b, *env, env->a_color.color_d);
	else if (pt_b.z >= 0)
		draw_a_line(pt_a, pt_b, *env, env->a_color.color_c);
	else
		draw_a_line(pt_a, pt_b, *env, env->a_color.color_b);
}

void		draw_y(int x, int y, t_env *env, t_coords pt_a)
{
	t_coords	pt_c;

	pt_c = trans_point((*env).array[y + 1][x], env);
	if (pt_a.z <= 0 && pt_a.z == pt_c.z)
		draw_a_line(pt_a, pt_c, *env, env->a_color.color_a);
	else if (pt_a.z == pt_c.z)
		draw_a_line(pt_a, pt_c, *env, env->a_color.color_d);
	else if (pt_a.z >= 0)
		draw_a_line(pt_a, pt_c, *env, env->a_color.color_c);
	else
		draw_a_line(pt_a, pt_c, *env, env->a_color.color_b);
}

int			draw(t_env *env)
{
	int			x;
	int			y;
	t_coords	pt_a;

	y = 0;
	clear_image(env->image);
	while ((*env).array[y])
	{
		x = 0;
		while ((*env).array[y][x])
		{
			pt_a = trans_point((*env).array[y][x], env);
			if ((*env).array[y][x + 1] != NULL)
				draw_x(x, y, env, pt_a);
			if ((*env).array[y + 1] != NULL)
				draw_y(x, y, env, pt_a);
			x++;
		}
		y++;
	}
	mlx_put_image_to_window(env->mlx, env->win, env->image->img, 0, 0);
	return (0);
}
