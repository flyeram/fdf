/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   press_key.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 11:15:33 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/12 13:36:30 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>
#include <mlx.h>
#include <libft.h>
#include <stdlib.h>

int			press_key(int key_code, t_env *env)
{
	ft_putnbr(key_code);
	ft_putchar('\n');
	if (key_code == 53)
		exit(3);
	if (key_code == 124)
		(*env).move->x += 3;
	if (key_code == 123)
		(*env).move->x -= 3;
	if (key_code == 125)
		(*env).move->y += 3;
	if (key_code == 126)
		(*env).move->y -= 3;
	if (key_code == 24)
		(*env).move->z += 0.5;
	if (key_code == 27 && (*env).move->z > 1)
		(*env).move->z -= 0.5;
	if (key_code == 78 && (*env).move->w > -3)
		(*env).move->w -= 0.01;
	if (key_code == 69 && (*env).move->w < 10)
		(*env).move->w += 0.01;
	if (key_code == 49)
		center(env);
	draw(env);
	return (0);
}
