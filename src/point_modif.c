/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   point_modif.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 10:58:21 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/12 11:40:17 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>

t_coords	trans_point(t_coords *pt, t_env *env)
{
	t_coords	new_pt;
	float		lenght;
	float		module;

	module = env->move->w;
	lenght = env->move->z;
	new_pt.z = pt->z;
	new_pt.x = env->move->x + (int)(lenght * pt->x) - (int)(lenght * pt->y);
	new_pt.y = env->move->y + (int)(lenght * pt->y) + (int)(lenght * pt->x) -
	(module * pt->z * 2);
	return (new_pt);
}

void		center(t_env *env)
{
	env->move->x = env->win_size.x / 2;
	env->move->y = env->win_size.y / 2;
}

void		set_origin(t_env *env)
{
	int		x;
	int		y;

	y = 0;
	while (env->array[y])
	{
		x = 0;
		while (env->array[y][x])
		{
			env->array[y][x]->x -= env->sizex_ar / 2;
			env->array[y][x]->y -= env->sizey_ar / 2;
			x++;
		}
		y++;
	}
}
