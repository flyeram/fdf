/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   constructor.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/23 13:01:05 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/08 15:13:19 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fdf.h>
#include <libft.h>
#include <mlx.h>
#include <stdlib.h>
#include <fcntl.h>

t_env		*constructor_env(int win_x, int win_y)
{
	t_env	*env;

	if (!(env = (t_env *)malloc(sizeof(t_env))))
		return (NULL);
	if (!((*env).mlx = mlx_init()))
		return (NULL);
	(*env).win = mlx_new_window((*env).mlx, win_x, win_y, "FdF");
	(*env).image = create_image((*env).mlx, win_x, win_y);
	(*env).array = NULL;
	if (!((*env).move = (t_coords *)malloc(sizeof(t_coords))))
		return (NULL);
	(*env).sizex_ar = 0;
	(*env).sizey_ar = 0;
	(*env).win_size.x = win_x;
	(*env).win_size.y = win_y;
	return (env);
}

int			constructor_get_data(t_list **list, char **array, t_env *env)
{
	int			len;
	int			x;
	t_coords	**tab_coords;
	t_list		*new_elem;

	x = 0;
	len = ft_arraylen(array);
	env->sizex_ar = (env->sizex_ar == 0 || env->sizex_ar == len) ? len : 0;
	if (env->sizex_ar == 0)
		return (0);
	if (!(tab_coords = (t_coords **)malloc(sizeof(t_coords) * len + 1)))
		return (0);
	tab_coords[len] = NULL;
	while (array[x])
	{
		if (!(tab_coords[x] = (t_coords *)malloc(sizeof(t_coords))))
			return (0);
		(*tab_coords[x]).x = x;
		(*tab_coords[x]).y = (*env).sizey_ar;
		(*tab_coords[x]).z = ft_atoi(array[x]);
		x++;
	}
	new_elem = ft_lstnew(tab_coords, sizeof(t_coords) * len + 1);
	ft_lsteadd(list, new_elem);
	return (1);
}

t_coords	***constructor_conv_list(t_list **list, int y)
{
	t_coords	***array;
	int			i;
	t_list		*next_elem;

	i = 0;
	if (!(array = (t_coords ***)malloc(y * sizeof(t_coords **) + 1)))
		return (NULL);
	array[y] = NULL;
	next_elem = (*list)->next;
	while (next_elem)
	{
		array[i] = (*(*list)).content;
		free(*list);
		*list = next_elem;
		next_elem = (*list)->next;
		i++;
	}
	array[i] = (*(*list)).content;
	*list = next_elem;
	free(*list);
	return (array);
}

int			constructor(t_env *env, char *fname)
{
	int		fd[2];
	char	*line;
	char	**array;
	t_list	*list;

	list = NULL;
	line = NULL;
	fd[0] = open(fname, O_RDONLY);
	while ((fd[1] = get_next_line(fd[0], &line)))
	{
		if (fd[1] == -1)
			return (0);
		array = ft_strsplit(line, ' ');
		if (!(constructor_get_data(&list, array, env)))
			return (0);
		(*env).sizey_ar++;
	}
	if (!((*env).array = constructor_conv_list(&list, (*env).sizey_ar)))
		return (0);
	return (1);
}
