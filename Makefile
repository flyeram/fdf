# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/26 18:04:58 by adubois           #+#    #+#              #
#    Updated: 2016/01/12 14:24:02 by tbalu            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf
CC = gcc
CFLAGS = -Wall -Werror -Wextra
INC_PATH = -I./inc/ -I./libft/ -I./minilibx_macos/
SRC_PATH = ./src/
SRCS = main.c dal/draw_a_line.c image.c constructor.c dal/draw_a_line_2.c \
dal/draw_a_line_3.c dal/draw_a_line_4.c dal/draw_a_line_5.c point_modif.c \
draw_point.c press_key.c
FLAGSMLX = -L minilibx_macos/ -lmlx -framework OpenGL -framework AppKit
FLAGSLIBFT = -L libft/ -lft
SRC = $(SRCS:%.c=$(SRC_PATH)%.c)
OBJ = $(SRC:%.c=%.o)

all: mlx lft $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(FLAGSLIBFT) $(FLAGSMLX)

mlx:
	make -C minilibx_macos/

lft:
	make -C libft/

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS) $(INC_PATH)

siclean:
	rm -f $(OBJ)

sifclean: siclean
	rm -f $(NAME)

sire: sifclean $(NAME)

clean:
	rm -f $(OBJ)
	make -C libft/ clean

fclean: clean
	rm -f $(NAME)
	make -C minilibx_macos/ clean
	make -C libft/ fclean

memclean:
	rm -rf *~

re: fclean all
