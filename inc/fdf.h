/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 12:57:32 by tbalu             #+#    #+#             */
/*   Updated: 2016/01/12 13:45:10 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

typedef struct	s_coords
{
	float		x;
	float		y;
	float		z;
	float		w;
}				t_coords;

typedef struct	s_image
{
	char		*cimg;
	void		*img;
	int			bpp;
	int			sizeline;
	int			endian;
	int			width;
	int			height;
}				t_image;

typedef struct	s_color
{
	unsigned int	color_a;
	unsigned int	color_b;
	unsigned int	color_c;
	unsigned int	color_d;
}				t_color;

typedef struct	s_env
{
	t_image			*image;
	void			*mlx;
	void			*win;
	t_coords		***array;
	int				sizex_ar;
	int				sizey_ar;
	t_color			a_color;
	t_coords		*move;
	t_coords		win_size;
	int				ac;
	char			**av;
}				t_env;

/*
**draw_a_line
*/

void			draw_horizontal(t_coords pt_a, t_coords pt_b, t_env env);
void			draw_vertical(t_coords pt_a, t_coords pt_b, t_env env);
void			draw_a_line(t_coords pt_a, t_coords pt_b,
				t_env env, unsigned int color);
void			loop_dxp_dyp_one(int *dx_dy, t_env env, t_coords pt_a,
				t_coords pt_b);
void			loop_dxp_dyp_two(int *dx_dy, t_env env,
				t_coords pt_a, t_coords pt_b);
void			loop_dxp_dyn_one(int *dx_dy, t_env env,
				t_coords pt_a, t_coords pt_b);
void			loop_dxp_dyn_two(int *dx_dy, t_env env,
				t_coords pt_a, t_coords pt_b);
void			loop_dxn_dyp_one(int *dx_dy, t_env env,
				t_coords pt_a, t_coords pt_b);
void			loop_dxn_dyp_two(int *dx_dy, t_env env,
				t_coords pt_a, t_coords pt_b);
void			loop_dxn_dyn_one(int *dx_dy, t_env env,
				t_coords pt_a, t_coords pt_b);
void			loop_dxn_dyn_two(int *dx_dy, t_env env,
				t_coords pt_a, t_coords pt_b);
void			draw_dxp_dyp(t_coords pt_a, t_coords pt_b,
				t_env env, int *dx_dy);
void			draw_dxp_dyn(t_coords pt_a, t_coords pt_b,
				t_env env, int *dx_dy);
void			draw_dxn_dyp(t_coords pt_a, t_coords pt_b,
				t_env env, int *dx_dy);
void			draw_dxn_dyn(t_coords pt_a, t_coords pt_b,
				t_env env, int *dx_dy);

/*
**image
*/

t_image			*create_image(void *mlx, int width, int height);
void			image_put_pixel(t_env env, int x, int y,
				unsigned int color);
void			clear_image(t_image *image);

/*
**constructor
*/

int				constructor(t_env *env, char *fname);
t_env			*constructor_env(int win_x, int win_y);

/*
**point_modif
*/

void			set_origin(t_env *env);
void			center(t_env *env);
t_coords		trans_point(t_coords *pt, t_env *env);

/*
**draw_point
*/

void			draw_x(int x, int y, t_env *env, t_coords pt_a);
void			draw_y(int x, int y, t_env *env, t_coords pt_a);
int				draw(t_env *env);

/*
**press_key
*/

int				press_key(int key_code, t_env *env);

#endif
